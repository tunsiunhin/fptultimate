particlesJS.load('particles-js', 'assets/file/particles.json', function() {});
if($(window).scrollTop() > 100)
	$('.navbar').addClass('active');	

$(window).scroll(function(){
	if($(this).scrollTop() > 100)
	{
		$('.navbar').addClass('active');	
	}	
	else{
		$('.navbar').removeClass('active');	
	}
});

$('a.page-scroll').bind('click', function(event) {
  var $anchor = $(this);
  $('html, body').stop().animate({
	scrollTop: $($anchor.attr('href')).offset().top - 50
  }, 500,'swing');
  event.preventDefault();
});



$('.service-item').click(function(e) {
	tool_info = $(this).find('.tool-info').text();
	tool_info = JSON.parse(tool_info);
	
	$('.tool-name').text(tool_info.tool_name);
	if(tool_info.video)
	{
		$('.loader').show();
		tool_video = '<iframe src="'+ tool_info.video +'" frameborder="0" allowfullscreen></iframe>';
	}
	else{
		tool_video = '';	
		$('.loader').hide();
	}
	if(tool_info.pay == 1){
		$('.detail-price').text(tool_info.price + '$/ Month');
		$('.btn-buy-now').attr('data-target','.tool-plan-'+tool_info.id).attr('data-plan','.plan-row-'+tool_info.id).prop('disabled',false).text('Buy now');
		
	}
	else{
		
		$('.detail-price').text('It is Free');
		$('.btn-buy-now').prop('disabled',true).text('Free');
		
	}
	$('#detail-icon').removeClass().addClass(tool_info.tool_icon);
	$('.btn-started').attr('href',tool_info.tool_link);
	$('.video-result').html(tool_video);
    $('.detail-modal').addClass('modal-active');
	$('body').css('overflow','hidden');
});

$('.list-tool a').click(function(e) {
	$('.list-tool a').removeClass('active');
	$('.plan-row').removeClass('active');
	$(this).addClass('active');    
	$($(this).attr('data-plan')).addClass('active');
});

$('.modal-hidden').click(function(e) {
    if($(e.target).closest('.detail-modal>div').length === 0) 
	{
  		 $(this).removeClass('modal-active');
		 $('body').css('overflow','auto');
	}
});

$('.btn-buy-now').click(function(e) 
{
  $('.detail-modal').removeClass('modal-active');
  $('body').css('overflow','auto');
  $('.list-tool a').removeClass('active');
  $('.plan-row').removeClass('active');
  $($(this).attr('data-target')).addClass('active');
  $($(this).attr('data-plan')).addClass('active');
  $('html, body').stop().animate({
	scrollTop: $('#pricing').offset().top - 50
  }, 1000,'swing');
});
$('.choose-plan').click(function(){alert('ok');});