<?php
class Curl{
	public function call($url, $param = '', $header_custom = '', $cookie='')
	{
		$header = array('Accept: application/json', 'Content-Type: application/json');
		if($header_custom)
			$header = $header_custom;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, "'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1'");
		if($param){
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$param);
		}
		if($cookie){
			curl_setopt($ch, CURLOPT_COOKIEFILE, getcwd().'/'.$cookie);
			curl_setopt($ch, CURLOPT_COOKIEJAR, getcwd().'/'.$cookie);
		}
		$res = curl_exec($ch);
		curl_close($ch);
		return $res;
	}
	public function fb_call($path, $method = 'GET', $data = NULL)
	{
		
		$url = 'https://graph.facebook.com/'.$path;
		$headers = array('Accept: application/json', 'Content-Type: application/json', );
		$ch    = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		switch($method) {
            case 'GET' :
                break;
            case 'POST' :
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case 'PUT' :
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case 'DELETE' :
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
        }	
		$response  = curl_exec($ch);
		curl_close($ch);
		return json_decode($response, true);	
	}
}
?>