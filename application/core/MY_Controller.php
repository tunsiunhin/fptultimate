<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller
{	
	public $user_id;
	public $user_email;
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		session_start();
		$this->load->library(array('curl'));
		$this->load->helper(array('layout_helper','string','cookie'));
		$this->check_cookie();		
 	}	
	
	protected function view($view,$userlayout=true)
	{
		if($userlayout==true){
			$this->data['view'] = $view;
			$this->load->view('layout/main',$this->data);	
		}
		else
			$this->load->view($view,$this->data);	
	}
	
	private function check_cookie()
	{
		
		if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_email'])){
			$this->unauthorized();
		}
		$this->user_id    = $_SESSION['user_id'];
		$this->user_email = $_SESSION['user_email'];
	}
	
	private function unauthorized()
	{
		header('HTTP/1.1 401 Unauthorized', true, 401);
		$veriry_url = urlencode(base_url().'authentication?redirect='.current_url());
		echo'<script>';
			echo'location.href="https://auth.fptultimate.com?redirect='.$veriry_url.'"';
		echo'</script>';
		die;		
	}
}
?>