<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('US/Pacific');

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['default_controller']   = 'home/index';
$route['authentication']       = 'home/authentication';
$route['token']		           = 'autoengage/index';
$route['autoengage']           = 'landing_page/landing_engage';
$route['adssuccess']		   = 'landing_page/landing_adssuccess';
$route['fanpagebuilder']	   = 'landing_page/landing_fanpagebuilder';
$route['kingtarget']		   = 'landing_page/landing_kingtarget';
$route['instadaily']		   = 'landing_page/landing_instadaily'; 