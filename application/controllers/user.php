<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends MY_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	public function profile()
	{
		$this->data['tools'] = $this->db->where('status >',0)->get('tools')->result_array();
		$this->data['user_actives'] = $this->db->where('user_id',$this->user_id)->get('user_actives')->result_array();
		
		$this->view('profile');
	}
	public function change_pass()
	{
		$pass_key  = 'u67BxWe';
		$old_pass  = $this->input->post('old_pass');	
		$new_pass  = $this->input->post('new_pass');	
		$confirm_pass  = $this->input->post('confirm_pass');
		
		if(strlen($new_pass) < 6){
			exit('{"error":1,"message":"The Password field must be at least 6 characters in length"');	
		}
		if(strcmp($new_pass,$confirm_pass) !== 0){
			exit('{"error":1,"message":"The Confirm Password field does not match the Password field"}');	
		}
		$param = array(
			'user_email' => $this->user_email,
			'user_pass' => md5($pass_key.$old_pass)
		);	
		$check = $this->db->where($param)->get('users')->row_array();
		if(!$check){
			exit('{"error":1,"message":"Old password is wrong"}');
		}
		$new_password = md5($pass_key.$new_pass);
		$this->db->where('id',$check['id'])->set(array('user_pass'=>$new_password,'user_changepass'=>0))->update('users');
		echo'{"success":1}';
	}
	
	public function logout()
	{
		session_start();
		$helper = array_keys($_SESSION);
		foreach ($helper as $key){
			unset($_SESSION[$key]);
		}
		$redirect = urlencode(base_url().'authentication');
		redirect('https://auth.fptultimate.com/logout?redirect='.$redirect);	
	}
}
?>