<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
	
	public function index()
	{
		$data['view'] = 'landingpage/fptultimate.php';
		$this->load->view('layout/main',$data);
	}
	public function authentication()
	{	
		session_start();
		$auth_token = 'i1eZRPef5QEmyBfWM1Tx';
		$info  = $this->input->get('info');
		
		$token = $this->input->get('token');
		
					
		$verify = sha1($auth_token.$info);
		
		if(strcmp($token,$verify) == 0)
		{
			$user_info = json_decode(base64_decode($info),true);
			
			if($user_info['expire'] < time() - 1800)
				exit('Unauthorized');
				
			$_SESSION['user_id']     = intval($user_info['user_id']);
			$_SESSION['user_email']  = $user_info['user_email'];
			if($this->input->get('redirect'))
				redirect($this->input->get('redirect'));
			else
				redirect(base_url().'user/profile');	
		}
		else{
			exit('Unauthorized');
		}
		
	}
	public function test()
	{
		echo '1';
	}
}
?>