<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Autoengage extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library(array('curl'));
	}
	
	public function index()
	{
        $code = $this->input->get('code');
		$client_id = '2025013521064541'; // Server
		$client_secret = 'bdbb9ee255ba1b2f41858b875164776a'; // Server
		$data = array();
		//$client_id = '1514349308648287'; // Localhost
		//$client_secret = '5bd645fab91c2abb9ff3f68e82404d75'; // Localhost
		
		if(!$code)
		{
			redirect(base_url());
			die();
		}
				
        $redirect = urlencode(base_url().'autoengage');
        $path = 'oauth/access_token?client_id='.$client_id.'&redirect_uri=' . $redirect . '&client_secret='.$client_secret.'&code='.$code;
		$res = $this->curl->fb_call($path);
        if(!isset($res['access_token']))
		{	
			echo 'Error! Can not get access token. Please try again';
			die();
        }
		
		$data['redirect'] = 'https://autoengage.pro/fbapi/getToken?access_token='.$res['access_token'];
		$this->load->view('redirect',$data);
		
	}
}
?>