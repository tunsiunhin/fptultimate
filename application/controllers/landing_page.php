<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Landing_page extends CI_Controller {
	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
	public function landing_engage()
	{
		$this->load->view('landingpage/autoengage');
	}
	public function landing_adssuccess()
	{
		$this->load->view('landingpage/adssuccess');
	}
	public function landing_kingtarget()
	{
		$this->load->view('landingpage/kingtarget');
	}
	public function landing_instadaily()
	{
		$this->load->view('landingpage/instadaily');
	}
}
?>