<style>
h2{
	
}
</style>
<div id="home">	
 	<div class="container" style="position:relative;z-index:10">
        <div class="row"> 
            <div class="col-md-7 text-demo">
                <h1 class="title">Internet Marketing Software</h1>
                <p class="description" style="color:rgba(255,255,255,.68);">Spend less time worrying about repeated works and more focusing on your product</p>
                <div class="action"> 
                    <a class="btn  btn-transparent" href="#products"> Read More</a>
                    <a class="btn btn-transparent" href="#contacts"> Contact Us</a>
                </div>
            </div>
            <div class="col-md-5">
            	<div class="video-demo">
                
                </div>
            </div>  
        </div>    
    </div>
    <div id="particles-js"></div>	
</div>

<div id="products">
 <div class="container">
     <div class="text-center product-intro">
        <h2>What are we providing</h2>
        <p>We are constantly developing products to serve customers in the best way.</p>
     </div>
     <div class="row text-center product-info"> 
     	<div class="col-md-3">
          <div class="service-item">
        	<a href="https://adssuccess.com" target="_blank"><i class="fa fa-search"></i></a>
            <a href="https://adssuccess.com" target="_blank"><h4 class="service-heading">Adssuccess</h4></a>
            <p class="service-description">Research and spy the products ads on Facebook</p>
          </div>  
        </div>
     	<div class="col-md-3">
          <div class="service-item">
       
        	<a href="https://kingtarget.pro" target="_blank"><i class="fa fa-crosshairs"></i></a>
            <a href="https://kingtarget.pro" target="_blank"><h4 class="service-heading">King Target</h4></a>
           <p class="service-description">Kingtarget helps you analytics audience and target from competitor Facebook post</p>
          </div>  
        </div>
     	<div class="col-md-3">
          <div class="service-item">
          
        	<a href="https://fanpagebuilder.pro" target="_blank"><i class="fa fa-line-chart"></i></a>
            <a href="https://fanpagebuilder.pro" target="_blank"><h4 class="service-heading">Build Page</h4></a>
            <p class="service-description">Find contents from Facebook, Youtube, Pinterest and auto post to your Facebook page</p>
          </div>  
        </div>
     
      
     	<div class="col-md-3">
          <div class="service-item">
          	
        	<a href="https://fptultimate.com/autoengage" target="_blank"><i class="fa fa-commenting-o"></i></a>
            <a href="https://fptultimate.com/autoengage" target="_blank"><h4 class="service-heading">Auto Engage</h4></a>
            <p class="service-description">Auto reply and send message to anyone who comments on your Facebook post</p>
          </div>  
        </div>
     
     
     </div>  </div><!---End Container ---->   
</div>

<div id="contacts">
 <div class="container text-center">
   <div class="contact-intro">
   	<h2>Contact Us</h2>
   </div>
   <div class="row contact-info">
        <div class="col-sm-3 col-md-3 item">
            <div style="cursor:pointer" onclick="window.open('https://facebook.com/nguyenvuteam');">
                <div><i class="fa fa-facebook-square"></i></div>
                <div class="des">
                    Fanpage
                </div>
            </div>
        </div>	
        
        <div class="col-sm-3 col-md-3 item">
            <div style="cursor:pointer" onclick="$('.chat_fb').click();">
                <div><i class="fa fa-comment"></i></div>
                <div class="des">
                    Chat with Support team
                </div>
            </div>
        </div>	
        
        <div class="col-sm-3 col-md-3 item">
            <div>
                <div><i class="fa fa-phone"></i></div>
                <div class="des">
                    +841649761848
                </div>
            </div>
        </div>	
        
        <div class="col-sm-3 col-md-3 item">
            <div>
                <div><i class="fa fa-envelope"></i></div>
                <div class="des">
                    fptultimate@gmail.com
                </div>	
            </div>
        </div>
        </div>
 </div>
</div>



<script type="text/javascript" src="../assets/js/particles.js"></script>  
<script>
	particlesJS.load('particles-js', 'assets/file/particles.json', function() {});
</script>  