<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:type" content="website" />
<meta property="og:image" content="<?=base_url()?>assets/img/demo.jpg" />
<meta property="og:url" content="<?=base_url()?>" />
<meta property="og:title" content="FACEBOOK ADS SPY" />
<meta property="og:description" content="Find Ideas For Your Products" />
<title>Adssuccess - Facebook Ads Spy</title>

<base href="<?=base_url()?>" >
<link  rel="icon" href="<?=base_url()?>assets/img/favicon.ico">
<link rel="stylesheet" href="<?=base_url()?>/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/font/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/css/landingpage_adssuccess.css?v=<?=time()?>" />

<script type="text/javascript" src="<?=base_url()?>assets/js/jQuery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
</head>

<body>

<header class="navbar navbar-default navbar-fixed-top header">
 <div class="container">
   <div class="navbar-header"> 
   	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> 
    <a class="navbar-brand logo" itemprop="headline" href="https://goo.gl/xf8LPy"><img src="../assets/img/logo3.png" width="250" /> </a>
  </div>
  
  <nav class="collapse navbar-collapse top-menu" role="navigation" id="navbar">
  	
     
    <ul class="nav navbar-nav navbar-right">
    	<li><a class="wide hidden-sm hidden-xs" href="https://goo.gl/xf8LPy" title="Log in"> <i class="fa fa-user"></i> Login </a> 
        	<a class="narrow hidden-lg hidden-md" href="https://goo.gl/xf8LPy" title="Log in"> <i class="icon icon-user"></i> Login </a>
        </li>
        <li style="padding-top:10px"> 
        	<button class="wide hidden-sm hidden-xs btn btn-warning btn-df" type="button" onclick="location.href='https://goo.gl/xf8LPy'"> Free Trial </button> 
            <button class="narrow hidden-lg hidden-md btn btn-sm btn-warning btn-flat btn-raised btn-small" type="button" onclick="location.href='https://goo.gl/xf8LPy'" style="margin-left:25px"> Free Trial </button>
        </li>
     </ul>
     
     <div class="menu-top-menu-container navbar-right">
     <ul id="menu-top-menu" class="nav navbar-nav">
      <li class="menu-features"><a href="#features" class="page-scroll">Features</a></li>
      <li class="menu-features"><a href="#pricing" class="page-scroll">Pricing</a></li>
      <li class="menu-features"><a href="#help" class="page-scroll">Helps</a></li>
     </ul>
    </div>
  </nav>
 </div>	
</header>

<div class="side-1">
 <div class="container">
    <h1 class="title">Find Ideas For Your Products</h1>
    <p class="description" style="color:rgba(255,255,255,.68);">Adsuccess is a statistical tool of facebook advertising, to help find and research the product on facebook easier</p>
    <div class="text-center side-1-action"> 
    	<button class="btn btn-warning btn-solid btn-padding-7 h-margin-2-0 h-margin-0-2 w-210" type="button" onclick="location.href='https://goo.gl/xf8LPy'"> Start Free Trial </button>
        <button href="#pricing" class="btn btn-transparent btn-solid btn-padding-9 h-margin-2-0 h-margin-0-2 w-210 page-scroll" type="button"> View Pricing </button></div>
    <div>
      <div style="padding:10px 10px 0 10px;">
       <div class="video-demo">
       	<img width="100%" src="../assets/img/demo.jpg" />
     	 
       </div>  
       
      </div>
    </div>
 </div>
</div>

<div id="features" class="side-2 text-center">
  <div class="container">
  	<h2 class="title-2" style="color:#333">Adsuccess Features to Grow Your Company</h2>
    <p class="description">Adsuccess is a statistical tool of facebook advertising, to help find and research the product on facebook easier</p>
    <div class="row">
      	<div class="col-md-4 text-center item"> 
        	<i class="fa fa-clock-o fa-2x"></i>
            <div class="text-center"> <br>
             	<h5 class="text-info media-heading"> 
            		Find Latest And Trending ads
            	</h5>
                <p class="text-padding-1">Search all ads the latest and most prominent with just a click.</p>
            </div>
       </div>
       
       <div class="col-md-4 text-center item"> 
        	<i class="fa fa-sliders fa-2x"></i>
            <div class="text-center"> <br>
             	<h5 class="text-info media-heading"> 
            		Search and filter Ads
            	</h5>
                <p class="text-padding-1">Filter ads by keywords, date and interactive views.</p>
            </div>
       </div>
       <div class="col-md-4 text-center item"> 
        	<i class="fa fa-search fa-2x"></i>
            <div class="text-center"> <br>
             	<h5 class="text-info media-heading"> 
            		Find Mutual Page
            	</h5>
                <p class="text-padding-1">Search similar page from a ads.</p>
            </div>
       </div>
    </div><!--End Row-->
    
  </div>
</div>



<div id="pricing"  class="side-3">
 <div class="container">
 	<div>
 		<h2 class="title-2" style="color:#fff">product research was never so simple</h2>
    </div>
    
    <div class="row" style="margin-top:30px;">
     <div class="col-md-4 col-sm-5 col-xs-12 col-push-4 col-md-push-2 col-sm-push-1">
        <div class="text-center buy-price-item">
          <h4 style="">Monthly</h4>
          <div class="info">
            <p class="price">
            	$97
            </p>
          </div>
          <div style="padding:30px 20px;background:#fff">
         



          <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="CB2UL8WXEHZDU">
            <input type="image" src="../assets/img/buynow.png" width="200" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
           </form>

          
          </div>   
        </div>
     </div>

     <div class="col-md-4 col-sm-5 col-xs-12  col-md-push-2 col-sm-push-1">
        <div class="text-center buy-price-item">
          <h4 style="">6 Monthly</h4>
          <div class="info">
            <p class="price">
            	$397
            </p>
          </div>
          <div style="padding:30px 20px;background:#fff">
          <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="T6VFZZFNDZYNS">
<input type="image"  src="../assets/img/buynow.png" width="200" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>

          </div>   
        </div>
     </div>
    
    </div>
 
    <div class="" style="margin-top:50px;">
    	<div style="text-align:center;padding:20px 0;">
            <h3 style="font-size:28px;font-weight:500;padding-bottom:20px;color:#fff;">Đối với khách hàng tại việt nam không có tài khoản thanh toán vui lòng chuyển về một trong các địa chỉ ngân hàng sau</h3>
            <div class="viet-pay" style="width:700px;max-width:100%;padding:20px 0;background:#fff;display:inline-block">
            <h4 style="margin-bottom:20px;font-size:24px;">Hệ thống tài khoản</h4>
            <div>
            	<p>* Gói 1 tháng: <b>2.150.000</b> vnđ</p>
                <p style="margin-top:5px">* Gói 6 tháng: <b>8.800.000</b> vnđ </p>
            </div>
             <div>
             	<p><label>1. </label> <span style="margin-left:5px;">Ngân Hàng ACB</span></p>
             	<p style="padding-left:25px"><span>Số tài khoản <b>666698</b></span>, <span>Vũ Duy Nguyên - Nguyễn Khánh Toàn, Hà Nội</span></p>
             </div>
             
             <div>
             	<p><label>4. </label> <span style="margin-left:5px;">PAYONEER</span></p>
             	<p style="padding-left:25px"><b>haanh@dongphuc.orgv  </b></p>
             </div>
            </div>
        </div>
    </div>	
 </div> <!---End Container ---->   
</div>

<div style="padding:80px 0;background:#fff;" id="help">
 <div class="container text-center">
   <h2 style="color:#444;margin-bottom:50px">CONTACT US</h2>
   <div class="clearfix contact">
            <div class="col-md-3 item">
                <div style="cursor:pointer" onclick="window.open('https://facebook.com/nguyenvuteam');">
                    <div><i class="fa fa-facebook-square"></i></div>
                    <div class="des">
                    	Go to fan page
                    </div>
                </div>
            </div>	
            
            <div class="col-md-3 item">
                <div onclick="parent.LC_API.open_chat_window({source:'minimized'})">
                    <div><i class="fa fa-comment"></i></div>
                    <div class="des">
                    	Chat with Support team
                    </div>
                </div>
            </div>	
            
            <div class="col-md-3 item">
                <div>
                    <div><i class="fa fa-phone"></i></div>
                    <div class="des">
                    	+841649761848
                    </div>
                </div>
            </div>	
            
            <div class="col-md-3 item">
                <div>
                    <div><i class="fa fa-envelope"></i></div>
                    <div class="des">
                    	fptultimate@gmail.com
                    </div>	
                </div>
            </div>
            </div>
 </div>
</div>

<!--<div style="min-height:300px;padding:30px 0">
	<div class="container text-center" style="">
		<div class="fb-comments" data-href="https://adsuccess.fptultimate.com/" data-width="100%" data-numposts="10"></div>
  	
	</div>
</div>-->



<footer>
	<div class="container">
    	
    	<ul class="f-menu">
        	<li><a href="#features" class="page-scroll">Home</a></li>
        	<li><a href="#features" class="page-scroll">Features</a></li>
            <li><a href="#pricing" class="page-scroll">Pricing</a></li>
            <li><a href="#help" class="page-scroll">Helps</a></li>
        </ul>
    </div>
</footer>

<style>#cfacebook{position:fixed;bottom:0px;right:50px;z-index:9999;width:300px;height:auto;box-shadow:0 2px 15px 0 rgba(0,0,0,.2);border-top-left-radius:2px;border-top-right-radius:2px;overflow:hidden;}#cfacebook .fchat{float:left;width:100%;height:270px;overflow:hidden;display:none;background-color:#fff;}#cfacebook .fchat .fb-page{margin-top:-130px;float:left;}#cfacebook a.chat_fb{float:left;padding:0 25px;width:300px;color:#fff;text-decoration:none;height:40px;line-height:40px;text-shadow:0 1px 0 rgba(0,0,0,0.1);background-color:#3a5795;border:0;border-bottom:1px solid #133783;z-index:9999999;margin-right:12px;font-size:18px;}
#cfacebook a.chat_fb:hover{color:yellow;text-decoration:none;}</style>
  <script>
  jQuery(document).ready(function () {
  jQuery(".chat_fb").click(function() {
jQuery('.fchat').toggle('fast');
  });
  });
  </script>
  <div id="cfacebook">
  <a href="javascript:;" class="chat_fb" onclick="return:false;"><i class="fa fa-facebook-square"></i> Message Us</a>
  <div class="fchat">
  <div class="fb-page" data-tabs="messages" data-href="https://www.facebook.com/nguyenvuteam" data-width="300" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>
  </div>
  </div>
<script>
$('.page-scroll').bind('click',function(e)
{
	 var $anchor = $(this);
  $('html, body').stop().animate({
	scrollTop: $($anchor.attr('href')).offset().top - 50
  }, 500,'swing');
  e.preventDefault();
	

});
</script>
<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-100968812-1', 'auto');
	  ga('send', 'pageview');
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>



<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '681126728746824'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=681126728746824&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
    
</body>
</html>
