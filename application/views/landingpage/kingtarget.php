<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:type" content="website" />
<meta property="og:image" content="assets/img/demo.jpg" />
<meta charset="utf-8">
<meta property="og:url" content="" />
<meta property="og:title" content="Internet Marketing Software" />
<meta property="og:description" content="Provide solutions for internet marketing" />
<title>Kingtarget.pro - Công cụ phân tích đối tượng</title>
<link  rel="icon" href="<?=base_url()?>assets/img/favicon.ico">
<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet">
<link rel="stylesheet" href="<?=base_url()?>assets/css/app.css?v=<?=time()?>">
<script type="text/javascript" src="<?=base_url()?>assets/js/jQuery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
</head>
<body>
    <header class="navbar navbar-default navbar-fixed-top header">
     <div class="container" >
       <div class="navbar-header"> 
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> 
        <a class="navbar-brand logo" itemprop="headline" href=""><img height="30" src="<?=base_url()?>assets/img/kingtarget/logo_full.png" /></a>
      </div>
      
      <nav class="collapse navbar-collapse top-menu" role="navigation" id="navbar">
         <div class="navbar-right">
            <ul style="padding-left:10px">
                 <li style="padding-top:10px;padding-right:15px;list-style:none;"> 
                    <a class="hidden-sm hidden-xs btn btn-warning btn-df" href="user/profile">Start Free Trial</a> 
                    <a class="hidden-lg hidden-md btn btn-warning btn-df" href="user/profile">Contact us</a> 
                </li>
            </ul>
         </div>
        
      </nav>
     </div>	
    </header>

	<div class="wrapper">

      	<div class="page_1">
    		<div class="container main-content">
                <h1><b>Kingtarget</b></h1>
				<h2>Công Cụ Phân Tích Đối Tượng</h2>
                <div style="display:inline-block;border:10px solid #333;border-radius:5px;">
    			<iframe width="854" height="480" src="https://www.youtube.com/embed/bzdleWIcLHQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
               </div> 
		
             	<h2><a target="_blank" href="https://kingtarget.pro" class="btn btn-success btn-lg btn-login">Đăng Nhập Để Sử Dụng Tool</a></h2>
	      </div>
           </div>

        <div class="page page_2">
            <div class="container">
                <div class="step-1 col-md-4 col-sm-12">
                    <h1 class="step-titel">Step #1</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Đăng nhập Kingtarget</p>
                    <p class="step-description">Để sử dụng tool các bạn cần phải tạo tài khoản, đăng nhập vào hệ thống.</p>
                </div>
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="<?=base_url()?>assets/img/page1_1.png">
                </div>
            </div>
        </div>

         <div class="page page_3">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="<?=base_url()?>assets/img/page1_2.png">
                </div>

                 <div class="step-2 col-md-4 col-sm-12">
                    <h1 class="step-titel">Step #2</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Lấy access token</p>
                    <p class="step-description">Bạn cần cài <a target='_blank' href="">extension</a> để lấy  Access Token. Bạn có thể xem video hướng dẫn lấy <a href="" target="_blank">tại đây</p>
                </div>
            </div>
        </div>

        <div class="page page_4">
            <div class="container">
                <div class="step-3 col-md-4 col-sm-12">
                    <h1 class="step-titel">Step #3</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Tạo Tiến Trình</p>
                    <p class="step-description">Bạn tạo tiến trình quét , điền các thông số cần thiết</p>
                </div>
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="<?=base_url()?>assets/img/page1_3.png">
                </div>

                 
            </div>
        </div>

         <div class="page page_5">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="<?=base_url()?>assets/img/page1_2.png">
                </div>

                 <div class="step-2 col-md-4 col-sm-12">
                    <h1 class="step-titel">Step #4</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Bắt Đầu Quét</p>
                    <p class="step-description">Quét và chờ đợi kết quả</p>
                </div>
            </div>
        </div>
    </div><!--End Wrap -->
    
	<footer id="footer" >
        <div class="container">
            <div class="col-md-12 copy-right">
                Copyright © 2017-2018 Fptultimate.com
            </div>
        </div>
	</footer>

</body>
</html>
    