<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:type" content="website" />
<meta property="og:image" content="<?=base_url()?>assets/img/demo.jpg" />
<meta property="og:url" content="<?=base_url()?>" />
<meta property="og:title" content="Internet Marketing Software" />
<meta property="og:description" content="Provide solutions for internet marketing" />
<title>Fptultimate.com - Internet Marketing Software</title>

<base href="<?=base_url()?>" >
<link  rel="icon" href="assets/img/favicon.ico">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/font/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/landingpage.css?v=<?=time()?>" />

<script type="text/javascript" src="assets/js/jQuery-2.1.4.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</head>

<body>
    <header class="navbar navbar-default navbar-fixed-top header">
     <div class="container" >
       <div class="navbar-header"> 
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> 
        <a class="navbar-brand logo" itemprop="headline" href=""><img height="34" src="assets/img/logox.png" /></a>
      </div>
      
      <nav class="collapse navbar-collapse top-menu" role="navigation" id="navbar">
         <div class="navbar-right">
            <ul style="padding-left:10px">
                 <li style="padding-top:10px;padding-right:15px;list-style:none;"> 
                    <a class="hidden-sm hidden-xs btn btn-warning btn-df" href="user/profile">My Account</a> 
                    <a class="hidden-lg hidden-md btn btn-warning btn-df" href="user/profile">Contact us</a> 
                </li>
            </ul>
         </div>
         <div class="menu-top-menu-container navbar-right">
             <ul id="menu-top-menu" class="nav navbar-nav">
              <li class="menu-features"><a href="#home" class="page-scroll">Home</a></li>
              <li class="menu-features"><a href="#products" class="page-scroll">Products</a></li>
              <li class="menu-features"><a href="about" class="page-scroll">About us</a></li>
              <li class="menu-features"><a href="#contacts" class="page-scroll">Contact us</a></li>
             </ul>
        </div>
      </nav>
     </div>	
    </header>
    
	<?php
    	$this->load->view($view);
    ?>
    
	<footer id="footer">
	<div class="container">
       	<div class="row">
            <div class="col-sm-3 col-md-3 info">
              <div>
                <a href=""><img src="assets/img/logo_s.png" height="140" /></a>
              </div>
            </div>
            <div class="col-sm-3 col-md-3 info">
            	<div>
                	<h5>Company</h5>
                    <ul>
                    	<li><a href="">About us</a></li>
                        <li><a href="">Privacy Policy</a></li>
                        <li><a href="">Terms of use</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 info">
            	<div>
                	<h5>Help</h5>
                	<ul>
                    	<li><a href="">Contact</a></li>
                        <li><a href="">Support</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 info">
                <div>
                    <h5>Connect with us</h5>
                    <ul class="social">
                        <ul>
                            <li><a href=""><i class="fa fa-facebook" style="background:#29487d"></i> Facebook</a></li>
                            <li><a href=""><i class="fa fa-google-plus" style="background:#db4437"></i> Youtube</a></li>
                        </ul>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div class="bottom">
    	&copy; Fptultimate.com, All Rights Reserved
    </div>
</footer>

<style>
	#cfacebook{position:fixed;bottom:0px;right:50px;z-index:9999;width:300px;height:auto;box-shadow:0 2px 15px 0 rgba(0,0,0,.2);border-top-left-radius:2px;border-top-right-radius:2px;overflow:hidden;}
	#cfacebook .fchat{float:left;width:100%;height:270px;overflow:hidden;display:none;background-color:#fff;}
	#cfacebook .fchat .fb-page{margin-top:-130px;float:left;}
	#cfacebook a.chat_fb{float:left;padding:0 25px;width:300px;color:#fff;text-decoration:none;height:40px;line-height:40px;text-shadow:0 1px 0 rgba(0,0,0,0.1);background-color:#3a5795;border:0;border-bottom:1px solid #133783;z-index:9999999;margin-right:12px;font-size:18px;}
	#cfacebook a.chat_fb:hover{color:yellow;text-decoration:none;}
</style>

<div id="cfacebook">
  <a href="javascript:;" class="chat_fb" onclick="return:false;"><i class="fa fa-facebook-square"></i> Message Us</a>
  <div class="fchat">
  <div class="fb-page" data-tabs="messages" data-href="https://www.facebook.com/nguyenvuteam" data-width="300" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>
  </div>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script> 

<script>
	$(document).ready(function () 
	{
		$(".chat_fb").click(function() {
			$('.fchat').toggle('fast');
		});
  	});
</script>
</body>
</html>
    