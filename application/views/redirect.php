<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Fptultimate.com </title>
</head>
<style>
	body
	{
		font-family:Tahoma, Geneva, sans-serif;
	}
	.container-wrapper
	{
		width:1170px;
		margin:auto;
	}
	.panel-redirect
	{
		border:solid 1px #ccc;
		width:70%;
		min-height:300px;
		margin:auto;
		margin-top:180px;
		border-radius:0;
		padding-left:40px;
		padding-right:40px;
		
	}
	.panel-notification
	{
		
		
	}
	.panel-notification>h3
	{
		border-bottom:dotted 1px #ccc;
		padding-bottom:12px;
		color:#0070e2;
		line-height:25px;
	}
	.panel-text
	{
		font-size:15px;
		color:#222;
		width:auto;
		min-height:90px;
		
	}
	.panel-button
	{
		text-align:center;
	}
	.btn-continue
	{
		text-decoration:none;
		border:solid 1.2px #ff8c00;
		padding:18px 55px;
		border-radius:3px;
		font-weight:500;
		text-align:center;
		color:white;
		background:#ff8c00;
		font-size:20px;
		margin-right:25px;
		text-transform:uppercase;
		
		
	}
	.link-redirect
	{
		color:#56575a;
		font-weight:600;
	}
	.error 
	{

		
	}
 </style>
<body>
	<div class="container-wrapper">
    	<div class="panel-redirect">
    		<div class="panel-notification">
            	<h3> Get Access Token Success </h3>
            </div>
    		<div class="panel-text">
            	
            </div>
            <div class="panel-button">
            	<a id="btn-continue" class="btn-continue" href="<?=$redirect?>"> Continue </a>
            </div>
   		</div>
    </div>
</body>

</html>