<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:type" content="website" />
<meta property="og:image" content="assets/img/fb_picture_2.png" />
<meta property="og:url" content="<?=base_url()?>autoengage" />
<meta property="og:title" content="Auto Engage" />
<meta property="og:description" content="Auto Reply Comment On Facebook" />
<title>Auto Engage - Fptultimate.com</title>
<link  rel="icon" href="assets/img/favicon.ico">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/font/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/app.css?v=<?=time()?>">
<script type="text/javascript" src="assets/js/jQuery-2.1.4.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

</head>

<body>
    <header class="navbar navbar-default navbar-fixed-top header">
     <div class="container" >
       <div class="navbar-header"> 
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> 
        <a class="navbar-brand logo" itemprop="headline" href=""><img height="34" src="assets/img/ae_logo.png" /></a>
      </div>
      
      <nav class="collapse navbar-collapse top-menu" role="navigation" id="navbar">
         <div class="navbar-right">
            <ul style="padding-left:10px">
                 <li style="padding-top:17px;padding-right:15px;list-style:none;"> 
                    <a class="hidden-sm hidden-xs btn btn-warning btn-df" style="font-weight:bold;" href="http://bit.ly/2GDRx0t">START FREE TRIAL</a> 
                    <a class="hidden-lg hidden-md btn btn-warning btn-df" style="font-weight:bold;" href="http://bit.ly/2GDRx0t">START FREE TRIAL</a> 
                </li>
            </ul>
         </div>
        
      </nav>
     </div>	
    </header>

	<div class="wrapper">

      	<div class="page_1">
    		<div class="container main-content">
                <h1><b>Auto Engage</b></h1>
				<h2>Tự động trả lời comment trên Facebook</h2>
                <div style="border:10px solid #333;border-radius:5px;" class="video-responsive">
    			<iframe width="1054" height="580" src="https://www.youtube.com/embed/bzdleWIcLHQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
               </div> 
				<div style="padding-top:50px;padding-bottom:40px;">
             	<a target="_blank" href="http://bit.ly/2GDRx0t" class="btn btn-success btn-lg" style="padding-top:20px;padding-bottom:20px;">Đăng Nhập Sử Dụng Công Cụ Ngay</a>
                </div>
	      </div>
           </div>

        <div class="page page_2">
            <div class="container">
                <div class="step-1 col-md-4 col-sm-12">
                    <h1 class="step-titel">Bước #1</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Đăng nhập</p>
                    <p class="step-description">Để sử dụng công cụ các bạn cần phải tạo tài khoản và đăng nhập vào hệ thống.</p>
                </div>
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="<?=base_url()?>assets/img/page1_1.png">
                </div>
            </div>
        </div>

         <div class="page page_3">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="<?=base_url()?>assets/img/page1_2.png">
                </div>

                 <div class="step-2 col-md-4 col-sm-12">
                    <h1 class="step-titel">Bước #2</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Get access token</p>
                    <p class="step-description">Lấy access token để hệ thống có quyền tương tác với Facebook</p>
                </div>
            </div>
        </div>

        <div class="page page_4">
            <div class="container">
                <div class="step-3 col-md-4 col-sm-12">
                    <h1 class="step-titel">Bước #3</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Cài đặt</p>
                    <p class="step-description">Tạo tiến trình trả lời cho các comment đã tồn tại hoặc cài đặt tự động trả lời các comment mới</p>
                </div>
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="<?=base_url()?>assets/img/page1_3.png">
                </div>

                 
            </div>
        </div>

        
    </div><!--End Wrap -->
    
    
    <style>
		#footer li{
			display:inline-block;
			margin-right:15px;	
		}
	</style>
	<footer id="footer" >
        <div class="container">
            <div class="col-md-12 copy-right">
            	<ul style="list-style:none">
                    <li>Copyright© 2017-2018 Fptultimate.com</li>
                    <li><a href="http://bit.ly/2HEBdvF">Terms Of Service</a></li>
                    <li><a href="http://bit.ly/2u1Whuc">Privacy Policy</a></li>
                </ul>
            </div>
            
        </div>
	</footer>

</body>
</html>
    