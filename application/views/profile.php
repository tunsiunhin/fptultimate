<style>
.main{
	background:#eee;
	min-height:900px;	
	padding-top:80px;	
}
.user-info img{
	width:200px;
	float:left;	
	margin-right:15px;
	border-radius:5px;
}
.user-info{
	margin-top:30px;
	height:260px;	
}
td{
	padding:20px 10px !important;	
}
</style>
<div class="main">
	<div class="container">
    	<h1>Profile</h1>
    	<div class="user-info">
        	<img src="assets/img/avatar.jpg" />
            <div class="txt">
            	<p>Email: <label><?=$_SESSION['user_email']?></label></p>
                <p>Phone: </p>
                <p>Facebook: </p>
                <p><a href="javascript:;" data-toggle="modal" data-target="#changePassModal"><i class="fa fa-pencil"></i> Change password</a></p>
            </div>
        </div>
        
        <div class="tool-info">
        	<table class="table">
            	<thead>
            	<tr>
                	<th>Name</th>
                    <th>Link</th>
                    <th>Expiry date</th>
                    <th>Status</th>
                    <th style="width:100px">Action</th>
                </tr>
                </thead>
                <tbody>
                	<?php foreach($tools as $row){
						$row['user_expire'] = 'N/a';
						foreach($user_actives as $active){
							if($active['user_tool'] == $row['id']){
								$row['user_expire'] = date('d/m/Y',$active['user_expire']);	
							}	
							
						}
						
						if($row['status'] == 0){
							$status = '<span class="label label-default">Unready</span>';	
						}
						else{
							$status = '<span class="label label-success">Active</span>';
						}
							
					?>
                    	<tr>
                        	<td><?=$row['tool_name']?></td>
                            <td><a href="<?=$row['tool_link']?>" target="_blank"><?=$row['tool_link']?></a></td>
                            <td><?=$row['user_expire']?></td>
                            <td><?=$status?></td>
                            <td>
                            	<button class="btn btn-sm btn-default">Extend</button>
                            </td>
                        </tr>
                    
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>		
</div>

<div id="changePassModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" style="margin-top: 150px !important;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times	"></i></button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <form class="" action="javascript:;" method="post">
            <div class="modal-body">
            	
            	<div class="pass-error" style="padding:10px;border:1px dotted #f00;margin-bottom:15px;border-radius:3px;display:none;color:red;"></div>	   
                <div class="form-group">   
                    <label>Old password</label>                        
                    <input type="password" class="form-control" name="old_pass" required>
                </div>   
                <div class="form-group">   
                    <label>New password</label>                        
                    <input type="password" class="form-control" name="new_pass" required>
                </div>   
                <div class="form-group">   
                    <label>Confirm password</label>                        
                    <input type="password" class="form-control" name="confirm_pass" required>
                </div> 
                           
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary change-pass"><i class="fa fa-send"></i> Save</button>
            </div>
            </form>    
        </div>
    </div>
</div> 
<script>
	$('.change-pass').click(function(e) {
      	var param = {
			'old_pass':$('input[name="old_pass"]').val(),
			'new_pass':$('input[name="new_pass"]').val(),
			'confirm_pass':$('input[name="confirm_pass"]').val()	
		};
		$this  = $(this);
		$.ajax({
			url:'user/change_pass',
			data:param,
			type:'post',
			dataType:"json",
			beforeSend: function(){
				$this.prop('disabled',true);	
			},
			success: function(res){
				$this.prop('disabled',false);
				if(res.error == 1)
					$('.pass-error').text(res.message).show();
				else if(res.success == 1)
					location.href="user/logout";
			}	
		});
    });
</script>