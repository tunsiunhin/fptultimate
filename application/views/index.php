<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property="fb:app_id" content="988531387883038" />
<meta property="og:site_name" content="fptultimate.com"/> 
<meta property="og:type" content="website" />
<meta property="og:image" content="<?=base_url();?>publics/img/banner.jpg" />
<meta property="og:url" content="<?=base_url();?>" />
<meta property="og:title" content="APPS FOR BUSINESS" />
<meta property="og:description" content="Spend less time worrying about repeated works and more focusing on your product" />
<link type="text/css" rel="stylesheet" href="publics/css/bootstrap.min.css"  />
<link type="text/css" rel="stylesheet" href="publics/font/font-awesome-4.7.0/css/font-awesome.min.css"  />
<link type="text/css" rel="stylesheet" href="publics/font/pe-7s/assets/Pe-icon-7-stroke.css"  />
<link type="text/css" rel="stylesheet" href="publics/css/main.css?<?=time();?>"  />

<script type="text/javascript" src="publics/js/jQuery-2.1.4.min.js"></script>
<title>App for bussiness</title>
</head>

<body>
<?php
	$this->load->view('layout/menu');
	$this->load->view('layout/detail-modal');
?>
<header id="header">
  <div class="container">
    <div class="intro-text">
        <h1 class="intro-lead-in">WELCOME TO APPS FOR BUSINESS</h1>
        <span class="intro-heading">Spend less time worrying about repeated works and more focusing on your product</span>
        <div class="header-buttons">
            <a href="<?=APP_URL?>" class="primary-button">Get Started</a>
            <a href="" class="secondary-button">Learn More</a>
        </div>

        <div class="header-dashboard"> 
            <img src="publics/img/header.jpg" class="dashboard" alt="" style="width:1000px">
        </div>
    </div>
  </div>
  <div id="particles-js"></div>	
</header>


<section id="services">
  <div class="container">
     <div class="text-center">
        <h2>What are we providing</h2>
        <p class="section-subheading">We are constantly developing products to serve customers in the best way.</p>
        
     </div>
     <?php foreach($tools as $key => $row){
		if($key == 0 || $key == 4 || $key == 8)	 
			echo '<div class="row text-center">';
     ?> 
     	<div class="col-md-3">
          <div class="service-item">
          	<div class="hidden tool-info"><?=json_encode($row)?></div>
        	<i class="<?=$row->tool_icon?>"></i>
            <h4 class="service-heading"><?=$row->tool_name?></h4>
           <p class="service-description"><?=$row->description?></p>
            <label><?php if($row->pay == 1) echo $row->price.'$ / Month'; else echo'FREE'; ?></label>
          </div>  
        </div>
     
     <?php 
	 	if($key == 3 || $key == 7 || $key == 11)	 
			echo '</div>';
	 } 
	 ?>
  </div>
</section>


<section id="pricing">
  <div class="container">
     <div class="text-center">
        <h2>Our Pricing</h2>
     </div>
     <ul class="list-tool">
     	<?php 
	 foreach($tools as $key => $row)
	 { 
	  if($row->pay == 1)
	  {
		  $key == 0 ? $active = 'active' : $active = '';
		  echo'<li><a href="javascript:;" class="tool-plan-'.$row->id.' '.$active.'" data-plan=".plan-row-'.$row->id.'">'.$row->tool_name.'</a></li>';
	  }
	 }?>
     </ul>
     <?php 
	 foreach($tools as $key => $row)
	 { 
	  if($row->pay == 1)
	  {
		$key==0 ? $active = 'active plan-row-'.$row->id : $active = ' plan-row-'.$row->id;
		echo'<div class="row text-center plan-row  '.$active.'">';	
		$plan = $plans[$row->id];

		foreach($plan as $p){	
       ?> 
       <div class="col-md-4">
            <div class="plan">
                <h4><?=$p->plan_name?></h4>
                <div>
                <p class="plan-price"><small>$</small><?=$p->price?></p>
                <p><a href="payment/<?=$p->id?>" class="btn btn-primary">Buy Now</a></p>
                </div>
            </div>
       </div>
        <?php 
		}
		echo '</div>';
	  } 
	 } 
	 ?>
      
  </div>
</section>


<section id="contact">
	<div class="container text-center">
    	<h2>Contacts Us</h2>
        <div class="row">
          <div class="col-md-5 text-left">
            <div class="social-contact">
            	<p><label>Facebook</label> <a href="https://www.facebook.com/khoa.truonganh.712" target="_blank">Vu Duy Nguyen</a></p>
                <p><label>Fan page</label> <a href="https://www.facebook.com/upteetools" target="_blank">Kiếm Ngàn $ mỗi ngày</a></p>
                <p><label>Phone</label> +84962734101</p>
                <p><label>Email</label> fptultimate@gmail.com</p>
            </div>    
          </div>
          
          <div class="col-md-7">
          	<div class="frm-contact">
             <div class="form-group">
                <input type="text" class="form-control" placeholder="Name" style="margin-right:20px;" />
                <input type="text" class="form-control" placeholder="Email" />
              </div>
              <div class="form-group">
                <textarea type="text" rows="7" class="form-control" placeholder="Message"></textarea>
              </div>
              <div class="from-group">
              	<button class="btn btn-primary btn-block">SEND MESSAGE</button>
              </div>
            </div>
          </div>   
        </div>  
    </div>
</section>
<?php
	$this->load->view('layout/footer');
?>

<script type="text/javascript" src="publics/js/particles.js"></script>
<script type="text/javascript" src="publics/js/smoothscroll.js"></script>
<script type="text/javascript" src="publics/js/main.js?<?=time();?>"></script>
</body>
</html>